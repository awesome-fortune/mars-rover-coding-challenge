class Grid
  attr_reader :grid

  def initialize(x_axis_max, y_axis_max)
    generate_grid(x_axis_max, y_axis_max)
  end

  def generate_grid(x_axis_max, y_axis_max)
    @grid = {}
    (0..x_axis_max).collect {|x| (0..y_axis_max).collect {|y| @grid["#{x},#{y}"] = Object.new}}
  end

end