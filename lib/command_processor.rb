class CommandProcessor
  attr_accessor :pending_commands, :active_command, :rover, :grid
  COMMAND_LIST_AND_DIRECTIONS_MAP = {
      :"L" => "DirectionLeft",
      :"R" => "DirectionRight",
      :"M" => "DirectionForward"
  }

  def initialize(file)
    convert_command_file_to_array(file)
    generate_grid
    process_command_list
  end

  def generate_grid
    grid_size = @pending_commands.shift(2)
    @grid = Grid.new(grid_size[0].to_i, grid_size[1].to_i)
  end

  def process_command_list
    until @pending_commands.empty?
      @rover = Rover.new(@grid, *@pending_commands.shift(3))

      @active_command = @pending_commands.shift(@pending_commands.take_while {|command| command =~ /[A-Z]/}.count)

      until @active_command.empty?
        interpret_command(active_command.shift).new(@rover, @rover.rover_grid_position, @rover.cardinal_direction)
      end

      @rover.get_rover_position_output
    end
  end

  private

  def interpret_command(command)
    Object.const_get(COMMAND_LIST_AND_DIRECTIONS_MAP[:"#{command}"])
  end

  def convert_command_file_to_array(file)
    begin
      @pending_commands = File.read(file).chars.reject! {|char| char == " " || char == "\n"}
    rescue IOError => e
      puts e.message
    end
  end
end