class DirectionBase
  def initialize(rover, grid_position, cardinal_point)
    @rover = rover
    @grid_position = grid_position.dup
    @cardinal_point = cardinal_point.dup

    execute
  end

  def execute
  end
end

class DirectionLeft < DirectionBase
  NEW_CARDINAL_DIRECTION = {
      :"N" => "W",
      :"E" => "N",
      :"S" => "E",
      :"W" => "S"
  }

  def execute
    @rover.update_cardinal_direction(get_new_cardinal_point(@cardinal_point))
  end

  private

  def get_new_cardinal_point(cardinal_point)
    NEW_CARDINAL_DIRECTION[:"#{cardinal_point}"]
  end
end

class DirectionRight < DirectionBase
  NEW_CARDINAL_DIRECTION = {
      :"N" => "E",
      :"E" => "S",
      :"S" => "W",
      :"W" => "N"
  }

  def execute
    @rover.update_cardinal_direction(get_new_cardinal_point(@cardinal_point))
  end

  private

  def get_new_cardinal_point(cardinal_point)
    NEW_CARDINAL_DIRECTION[:"#{cardinal_point}"]
  end
end

class DirectionForward < DirectionBase
  def execute
    move
  end

  private

  def move
    split_coordinates
    ["E", "W"].include?(@cardinal_point) ? move_on_x_axis : move_on_y_axis
    @rover.update_rover_grid_position(join_coordinates)
  end

  def move_on_x_axis
    case @cardinal_point
    when "E"
      @grid_position[0] += 1
    else
      @grid_position[0] -= 1
    end
  end

  def move_on_y_axis
    case @cardinal_point
    when "N"
      @grid_position[1] += 1
    else
      @grid_position[1] -= 1
    end
  end

  def join_coordinates
    @grid_position = @grid_position.collect(&:to_s).join(',')
  end

  def split_coordinates
    @grid_position = @grid_position.split(",").collect(&:to_i)
  end
end