require 'faker'

class Rover
  attr_accessor :grid, :rover_grid_position, :cardinal_direction

  def initialize(grid, current_x, current_y, current_cardinal_point)
    @grid = grid
    @rover_grid_position = "#{current_x},#{current_y}"
    @cardinal_direction = current_cardinal_point
  end

  def update_cardinal_direction(new_cardinal_direction)
    @cardinal_direction = new_cardinal_direction
  end

  def update_rover_grid_position(new_grid_position)
    @rover_grid_position = new_grid_position
  end

  def get_rover_position_output
    puts "=" * 20
    puts "Rover Name: #{Faker::Space.nasa_space_craft}"
    puts "#{@rover_grid_position.sub(',', ' ')} #{@cardinal_direction}"
  end

end