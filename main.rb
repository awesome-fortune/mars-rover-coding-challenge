require './lib/command_processor'
require './lib/direction'
require './lib/grid'
require './lib/rover'

welcome_message = "* Welcome to the Mars Rover Console *"

instructions =  <<~appmenu
How would you like to use the rover console?

1 - Load commands from file.
2 - Generate random commands for the rovers and see how things go.
0 - Exit the console.

appmenu

console_key_help = "Press [1] or [2] to select a mode for the rover console or [0] to exit."
console_usage_hint = "\nPress [1] to run the default command file, [2] to generate a random command file or [0] to exit.\n"

puts "*" * welcome_message.size
puts welcome_message
puts "*" * welcome_message.size
puts instructions
puts console_key_help

def generate_random_grid_boundries
  2.times.collect{ rand(0...20) }
end

def generate_movement_commands
  command_list = %w(L R M)
  commands = ''

  rand(5..20).times do
    commands << command_list[rand(0..2)]
  end

  commands
end

def generate_random_rovers(grid_boundries)
  cardinal_points = %w(N E S W)
  rover_data = []
  rand(1...10).times do
    rover_data << "#{rand(0..grid_boundries.first)} #{rand(0..grid_boundries.last)} #{cardinal_points[rand(0..3)]}"
    rover_data << "#{generate_movement_commands}"
  end

  rover_data
end

def random_mars_rover_situation
  grid_boundries = generate_random_grid_boundries
  rover_data = ''
  generate_random_rovers(grid_boundries).each{ |item| rover_data << item + "\n\n" }

  <<~filedata
  #{grid_boundries.first} #{grid_boundries.last}

  #{rover_data}  
  filedata
end

while (console_mode = gets.chomp) != "0"
  case console_mode
  when "1"
    default_commands_file_path = "#{Dir.pwd}/lib/instructions/default/rover_commands.txt"
    CommandProcessor.new(default_commands_file_path)

    puts console_usage_hint
  when "2"
    timestamp = Time.now.strftime('%Y_%m_%d_%H_%M_%S')
    rover_command_file = "#{Dir.pwd}/lib/instructions/custom/rover_commands-#{timestamp}.txt"
    
    begin
      File.open(rover_command_file, 'w'){ |file| file.write(random_mars_rover_situation)}
      CommandProcessor.new(rover_command_file)

      puts console_usage_hint
    rescue IOError => e
      puts e.message      
    end
  else
    puts "#{console_mode} is not valid input: #{console_key_help}"
  end
end