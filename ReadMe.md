# Mars Rover Coding Challenge
> This is a rendition of the challenge in Ruby

### Setting the project up on your machine
``` bash
git clone https://awesome-fortune@bitbucket.org/awesome-fortune/mars-rover-coding-challenge.git
cd mars-rover-coding-challenge && bundle install
```

### Running the program
ensure that you're in the mars-rover-coding-challenge directory
``` bash
ruby main.rb
```

### Running tests
``` bash
rspec --format doc
```

When the Mars Rover Console is up and running it will respond to the following user input
- Pressing `1` while in the console will execute the list of commands in the `/mars-rover-challenge/lib/instructions/default/rover_commands.txt`
- Pressing `2` will generate a file with random commands and execute the commands
- Pressing `0` will terminate the Mars Rover Console program

Commands in `/mars-rover-challenge/lib/instructions/default/rover_commands.txt` can be changed, the structure of a valid command file is as follows:
```
[grid_max_x_coordinate] [grid_max_y_coordinate]

[rover_current_x_coordinate_on_grid] [rover_current_y_coordinate_on_grid] [rover_current_cardinal_point_direction]

[rover_movement_command]

> NOTE: Multiple rovers can be deployed on a grid by adding more lines to the file regarding the rover's current position and the commands to be executed
```

#### Sample Input : Deploying 2 rovers on a grid
```
5 5

1 2 N

LMLMLMLMM

3 3 E

MMRMMRMRRM
```

### Sample Output 
```
Rover Name: Challenger
1 3 N
====================
Rover Name: Apollo
5 1 E
```