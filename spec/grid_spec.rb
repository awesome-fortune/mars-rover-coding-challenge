require './lib/grid'

describe 'Grid' do
    let(:surface){Grid.new(5,5)}

    context 'when instantiated' do

        it 'has each coordinate pair(grid block) as a standard Object' do 
			expect(surface.grid["5,5"].class).to eq Object 
        end
    end
end