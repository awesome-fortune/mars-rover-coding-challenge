require './lib/rover'
require './lib/direction'

describe 'Rover' do
    let(:surface){Grid.new(5,5)}
    let(:spirit){Rover.new(surface, "3", "3", "E")}
    let(:direction_left){DirectionLeft.new(spirit, spirit.rover_grid_position, spirit.cardinal_direction)}
    let(:direction_right){DirectionRight.new(spirit, spirit.rover_grid_position, spirit.cardinal_direction)}
    let(:direction_forward){DirectionForward.new(spirit, spirit.rover_grid_position, spirit.cardinal_direction)}
    
    context 'when instantiated' do

        it 'has information about it\'s whereabouts' do 
            expect(spirit.cardinal_direction).to eq "E"
            expect(spirit.rover_grid_position).to eq "3,3"
            expect(spirit.grid).to eq surface
        end

        it 'can turn left and update it\'s cardinal direction' do
            direction_left
            expect(spirit.cardinal_direction).to eq 'N'
        end

        it 'can turn right and update it\'s cardinal position' do
            direction_right
            expect(spirit.cardinal_direction).to eq 'S'
        end

        it 'can move forward n amount of steps on the current direction that it\'s facing' do
            direction_forward
            expect(spirit.rover_grid_position).to eq '4,3'
        end 
    end
end